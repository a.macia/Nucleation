import os
import sys
import numpy as np
import re


class input_reader():
    """
    Input reader class.

    It requires the file name and the type of file. Right now it is able to read
    xyz files and gulp .res files.
    It stores the atom names, coordinates and number of atoms.
    Work to do: add gin read function, maybe automatically read the file insted
    of using an if in main.py.
    """

    def __init__(self, file_name, type_file, num_atoms=0):
        """
        Default variables.
        file_name : name of the file without extension
        type_file : xyz / res / gin (now only xyz)
        """
        self.atom_names = []
        self.atom_coordinates = []
        self.num_atoms = num_atoms
        self.file_name = file_name
        self.type_file = type_file

    def read_xyz(self):
        """Atom coordinates reader for xyz."""
        input_file = open(self.file_name + '.' + self.type_file, 'r')
        lines = input_file.readlines()
        self.num_atoms = lines[0].split()[0]
        for i in range(2, len(lines)):
            self.atom_names.append(lines[i].split()[0])
            self.atom_coordinates.append([lines[i].split()[1],
                                          lines[i].split()[2],
                                          lines[i].split()[3]])

    def read_res(self):
        """Atom reader for res files."""
        with open(self.file_name + '.' + self.type_file, 'r') as input_file:
            for line in input_file:
                if 'cartesian' in line:
                    for i in range(self.num_atoms):
                        line2 = next(input_file)
                        self.atom_names.append(line2.split()[0])
                        self.atom_coordinates.append([line2.split()[2],
                                                      line2.split()[3],
                                                      line2.split()[4]])

    def write_xyz(self, name):
        """"Write the atom as an xyz."""
        output = []
        output.append(str(len(self.atom_names)))
        output.append(' ')
        for atom, coordinates in zip(self.atom_names, self.atom_coordinates):
            coordinates = [float(x) for x in coordinates]
            pcoord = "{:12.5f} {:12.5f} {:12.5f}".format(*coordinates)
            output.append("{:3}".format(atom) + pcoord)
        with open(name, 'w') as xyz_outfile:
            xyz_outfile.write("\n".join(output))


class parameter_reader():
    """
    Configuration run and properties reader.

    Default file name is conf.data.
    Right now, the properties it looks for in the conf file are:

    name of the input file (which will split to obtain the kind of file)
    temperature (K)
    velocity (A/ps)
    dispersion for the velocity
    cycles of additions
    atom types
    default atom type
    conditions for the rest of atom types

    If no atom types are necessaary, line can be empty and it will take it as there
    are none. If there are, below the Atypes line in conf.data the atom types must
    be present. These are stored in a list and the lenght of it will be the number
    of lines that will be read below, so if there are 3 atomtypes, three lines must
    be below them with the default atom types or the conditions to diferentiate them.
    If there is a dafault atom type, the line must contain in order:

    Atype default atom

    and the code will later convert all atom to Atype. The lines below must contain
    the rest of conditions which should be given as:

    Atomtype atom < lenght

    As for now it is the only condition which we are using.
    """

    def __init__(self, file_name='conf.data', temperature=10, velocity=10,
                 dispersion=1, cycles=1, input_file=None, input_type=None,
                 f_field='mod_ffsioh3.lib'):
        """Generating initial variables.

        atom_types and conditions are list, while the default_at is a dictionary
        """
        self.file_name = file_name
        self.temperature = temperature
        self.velocity = velocity
        self.cycles = cycles
        self.input_file = input_file
        self.input_type = input_type
        self.dispersion = dispersion
        self.atom_types = []
        self.shells = []
#        self.default_at = {}
        self.conditions = []
        self.target_stoichiometry = {}

    def read_configuration(self):
        """Read the configuration file.

        Stuff that needs to be added:
        """
        with open(self.file_name, 'r') as conf_file:
            for line in conf_file:
                if "INPUT" in line.upper():
                    file_type = next(conf_file).rstrip('\n').split('.')
                    self.input_file = file_type[0]
                    self.input_type = file_type[1]
                elif "FORCEFIELD" in line.upper():
                    self.f_field = next(conf_file)
                elif "TEMPERATURE" in line.upper():
                    self.temperature = float(next(conf_file))
                elif "CYCLES" in line.upper():
                    self.cycles = int(next(conf_file))
                elif "DESIRED VELOCITY" in line.upper():
                    self.velocity = float(next(conf_file))
                elif "VELOCITY DISPERSION" in line.upper():
                    self.dispersion = float(next(conf_file))
                elif "DESIRED STOICHIOMETRY" in line.upper():
                    target_stoichiometry = re.findall('[A-Z][a-z]*[1-9]*',
                                                      next(conf_file))
                    for i in target_stoichiometry:
                        split_line = re.match('([A-Z][a-z]*)([1-9])*', i).groups()
                        element = split_line[0]
                        value = split_line[1]
                        if value is not None:
                            self.target_stoichiometry[element] = int(value)
                        else:
                            self.target_stoichiometry[element] = 1
                elif "STOICHIOMETRY DISPERSION" in line.upper():
                    self.stoichiometry_disp = float(next(conf_file))
                elif "ATYPES" in line.upper():
                    for i in next(conf_file).split():
                        self.atom_types.append(i)
                    for i in range(len(self.atom_types)):
                        line2 = next(conf_file)
#                        if "DEFAULT" in line2.upper():
#                            self.default_at[line2.split()[2]] = line2.split()[0]
#                        else:
                        self.conditions.append(line2.split())
                if "SHELLS" in line.upper():
                    number_shells = int(next(conf_file))
                    for i in range(number_shells):
                        line2 = next(conf_file)
                        self.shells.append(line2.split())


class gulp_inputgen():
    """Input generator for gulp calculations.

    Work to do: add the relaxation code"""

    def __init__(self, configuration, main_particle, inc_particle,
                 forcefield='mod_ffsioh3.lib', name='gulp_input'):
        self.forcefield = forcefield
        self.configuration = configuration
        self.main_particle = main_particle
        self.inc_particle = inc_particle
        self.name = name
        self.main_particle_shells = main_particle
        self.inc_particle_shells = inc_particle
        self.atom_wshel = []
# Crash if the library doesn't exist
        if 'GULP_LIB' not in os.environ:
            print('Variable Gulp_lib no present, exiting')
            sys.exit()
# Looking for shells in the library file
        else:
            print('Found GULP_LIB')
            path = os.environ['GULP_LIB']
            with open(path + forcefield, 'r') as ff:
                for line in ff:
                    if 'SPECIES' in line.upper():
                        num_species = int(line.split()[1])
                        for i in range(num_species):
                            data = next(ff)
                            if 'SHEL' in data.upper():
                                self.atom_wshel.append(data.split()[0])
        print('Found the following atom types which require shell')
        print(self.atom_wshel)

    def md_calc(self):
        generated_input = open(self.name + '.res', 'w')
        generated_input.write('md conp molq\n')
        generated_input.write('name Run 1\n')
        generated_input.write('cartesian\n')
        shells = []
        # Convert all atoms to their atom types, if there are ATYPES.
        # S'hauria d convertir en una funcio global per a tots els calculs.
        for i in range(len(self.main_particle.atoms)):
            if self.main_particle.atoms[i] in self.configuration.default_at:
                self.main_particle.atoms[i] = self.configuration.default_at[self.main_particle.atoms[i]]
                self.check_condition(self.main_particle.atoms, self.main_particle.coordinates, self.configuration.conditions,i)
            else:
                self.check_condition(self.main_particle.atoms, self.main_particle.coordinates, self.configuration.conditions,i)

        for i in range(len(self.inc_particle.atoms)):
            if self.inc_particle.atoms[i] in self.configuration.default_at:
                self.inc_particle.atoms[i] = self.configuration.default_at[self.inc_particle.atoms[i]]
                self.check_condition(self.inc_particle.atoms, self.inc_particle.coordinates, self.configuration.conditions,i)
            else:
                self.inc_particle.atoms = self.check_condition(self.inc_particle.atoms, self.inc_particle.coordinates, self.configuration.conditions,i)

        # Writing atom coordinates
        for i in range(len(self.main_particle.atoms)):
            generated_input.write('{0:5} core {1:15.6f} {2:15.6f} {3:15.6f} \n'.format(self.main_particle.atoms[i], self.main_particle.coordinates[i,0], self.main_particle.coordinates[i,1], self.main_particle.coordinates[i,2]))
            if self.main_particle.atoms[i] in self.atom_wshel:
                    shells.append('{0:5} shel {1:15.6f} {2:15.6f} {3:15.6f} \n'.format(self.main_particle.atoms[i], self.main_particle.coordinates[i,0], self.main_particle.coordinates[i,1], self.main_particle.coordinates[i,2]))
        shells_main_p = len(shells)
        for i in range(len(self.inc_particle.atoms)):
            generated_input.write('{0:5} core {1:15.6f} {2:15.6f} {3:15.6f} \n'.format(self.inc_particle.atoms[i], self.inc_particle.coordinates[i,0], self.inc_particle.coordinates[i,1], self.inc_particle.coordinates[i,2]))
            if self.inc_particle.atoms[i] in self.atom_wshel:
                    shells.append('{0:5} shel {1:15.6f} {2:15.6f} {3:15.6f} \n'.format(self.inc_particle.atoms[i], self.inc_particle.coordinates[i,0], self.inc_particle.coordinates[i,1], self.inc_particle.coordinates[i,2]))

        for i in range(len(shells)):
            generated_input.write(shells[i])
        generated_input.write('\n')
        generated_input.write('ensemble npt\ntemperature {0}\ntimestep  0.001 ps\n'.format(self.configuration.temperature))
        generated_input.write('equilibrate 0.0000 ps\nproduction  6.000 ps\n')
        generated_input.write('sample 0.05 ps\nwrite_MD 0.005 ps\n')
        generated_input.write('dump every   1 gulp.res\noutput movie arc gulp.arc\n')
        generated_input.write('\n')
        generated_input.write('current_time 0.000 ps\n')
        # generated_input.write('aver 0.0 0.0 0.0 0.0 0.0 0.0 0.0\n')
        generated_input.write('velocities angs/ps\n')
        number_atom = 1
        for i in range(0, len(self.main_particle.atoms)):
            generated_input.write('{} {} {} {}\n'.format(number_atom, self.main_particle.velocities[i,0], self.main_particle.velocities[i,1], self.main_particle.velocities[i,2]))
            number_atom += 1
        for i in range(0, len(self.inc_particle.atoms)):
            generated_input.write('{} {} {} {}\n'.format(number_atom, -self.inc_particle.velocities[0], -self.inc_particle.velocities[1], -self.inc_particle.velocities[2]))
            number_atom += 1
        for i in range(0, len(shells)):
            if i < shells_main_p:
                generated_input.write('{} {} {} {}\n'.format(number_atom, 0.0, 0.0, 0.0))
                number_atom += 1
            else:
                generated_input.write('{} {} {} {}\n'.format(number_atom, -self.inc_particle.velocities[0], -self.inc_particle.velocities[1], -self.inc_particle.velocities[2]))
                number_atom += 1
        generated_input.write('\n')
        generated_input.write('library {}'.format(self.forcefield))
        generated_input.close()

    def check_condition(self, particle_atoms, particle_coordinates, inp_condition, position):
        '''Aquesta funcio ara mateix peta el inc particle'''
        # print(inp_condition)
        for condition in inp_condition:
            if particle_atoms[position][0] in condition[0][0]:
                if condition[2] == '<':
                    for number_2ndatom in range(len(particle_atoms)):
                        if condition[1] in particle_atoms[number_2ndatom]:
                            if np.linalg.norm(particle_coordinates[position, :] - particle_coordinates[number_2ndatom, :]) <= float(condition[3]):
                                particle_atoms[position] = condition[0]

        # print(particle_atoms)
        # print(type(particle_atoms))
        # print('Sortint de la funcio')
        return particle_atoms

def arc_tovmd(file_name, num_atoms, outfile='run_mov.xyz'):
    output_file = open(outfile, 'w')
    step = 1
    with open(file_name, 'r') as input_file:
        for line in input_file:
            if 'DATE' in line.upper():
                output_file.write('{0}\n'.format(num_atoms))
                output_file.write('Iteration: {0}\n'.format(step))
                for i in range(num_atoms):
                    data = next(input_file)
                    data = data.split()
                    output_file.write('{} {} {} {}\n'.format(data[6], data[1], data[2], data[3]))
                step += 1
    output_file.close()


class lammps_inputgen():
    """LAMMPS input generator."""

    def __init__(self, configuration, main_particle, inc_particle,
                 name='lmp_mpi -in '):
        """INITIALIZATION."""
        self.configuration = configuration
        self.main_particle = main_particle
        self.inc_particle = inc_particle
        self.name = name
        self.main_particle_shells = main_particle
        self.inc_particle_shells = inc_particle
        self.charges = {1: 0.658073, 2: 1.919810, 3: 1.429114, 4: 2.7226, 5: 1.3613,
                        6: -3.281110, 7: -2.767837}

    def md_calc(self):
        coordinate_file = open('coordinates.lmpdata', 'w')
        configuration_file = open('in.RDX', 'w')

        # Generation of the coordinates file
        a_types, a_coord_write = self.check_atoms() # S'ha d'escriure la funcio
        s = "\n"

        change_mol = len(self.main_particle.atoms)
        num = 0
        num_molecule = 1
        num_O = 0
        bonds = []
        a_velocities = np.append(self.main_particle.velocities, self.inc_particle.velocities, axis=0)
        for xyz, atom in zip(a_coord_write, a_types):
            num += 1
            s += '{0} {1} {2} {3:8.6f} {4:8.6f} {5:8.6f} {6:8.6f}\n'.format(num,
                                                                            num_molecule, atom, self.charges[int(atom)],
                                                                            *xyz)
            for shel in self.configuration.shells:
                if atom in shel:
                    num += 1
                    num_O += 1
                    s += '{0} {1} {2} {3:8.6f} {4:8.6f} {5:8.6f} {6:8.6f}\n'.format(num,
                                                                                    num_molecule, shel[1], self.charges[int(shel[1])],
                                                                                    *xyz)
                    if atom == 3:
                        bond_type = 2
                    else:
                        bond_type = 1
                    bonds.append([num-1, num, bond_type])
            if num == change_mol + num_O :
                num_molecule += 1

        coordinate_file.write('Nucleation running\n\n')
        coordinate_file.write('{0} atoms\n'.format(num))
        coordinate_file.write('{0} bonds\n'.format(num_O))
        coordinate_file.write("""0 angles
0 dihedrals

7 atom types
2 bond types
0 angle types
0 dihedral types

-100.0 100.0 xlo xhi
-100.0 100.0 ylo yhi
-100.0 100.0 zlo zhi


Masses

1 1.00794 # H
2 14.3995 # O core
3 14.3995 # O core
4 28.0855 # Si
5 24.3050 # Mg
6 1.5999  # O shell
7 1.5999  # O shell

Atoms
""")
        coordinate_file.write(s)
        coordinate_file.write("""
Bonds

""")
        for i in range(num_O):
            coordinate_file.write('{} {} {} {}\n'.format(i+1, bonds[i][2], bonds[i][0], bonds[i][1]))

        coordinate_file.write('\n')
        coordinate_file.write('Velocities\n')

        num = 0
        s ='\n'
        for vxyz, atom in zip(a_velocities, a_types):
            num += 1
            s += '{0} {1:8.6f} {2:8.6f} {3:8.6f}\n'.format(num,
                                                           *vxyz)
            for shel in self.configuration.shells:
                if atom in shel:
                    num += 1
                    s += '{0} {1:8.6f} {2:8.6f} {3:8.6f}\n'.format(num,
                                                                   *vxyz)

        coordinate_file.write(s)

        configuration_file.write("""# ------------- INITIALIZATION
units           metal # m=grams/mo, E=eV, T=K, etc
atom_style      full # sistema atomic amb carregues

# -------------- ATOM DEFINITION
read_data       coordinates.lmpdata # coordenades dels atoms
group mainparticle molecule 1

kspace_style ewald 1.0e-6
# ------------- FORCE FIELD DEFINITION
pair_style hybrid buck/coul/long/cs 15.0 100 morse 10
pair_coeff * * buck/coul/long/cs 0.0 1.0 0.0
pair_coeff 6 6 buck/coul/long/cs 15039.909 0.227708 0.0
pair_coeff 6 7 buck/coul/long/cs 6768.7644 0.245932 0.0
pair_coeff 7 7 buck/coul/long/cs 1688.1482 0.292545 0.0
pair_coeff 4 6 buck/coul/long/cs 8166.2632 0.193884 0.0
pair_coeff 5 6 buck/coul/long/cs 2014.61 0.247732 10.1938
pair_coeff 5 7 buck/coul/long/cs 2014.61 0.247657 10.1937
pair_coeff 4 7 morse 0.04589720 2.6598 2.33921    ## INtrabond Si O2 shell arreglar que ho fa petar tot


bond_style harmonic
bond_coeff 1 256.71027 0.0 # units eV/A^2
bond_coeff 2 160.84247 0.0 # units eV/A^2

# -------------- FF
neighbor        2 bin #skin (dist extra per les llistes) bin= tipo de construccio)
neigh_modify    one 5000
neigh_modify    every 5 delay 0 check yes #freq d'actualitzacio de les llistes
thermo 100
""")
        configuration_file.write("velocity mainparticle create {} 123456789".format(self.configuration.temperature))
        configuration_file.write("""# -------------- MD RUN
fix 1 all box/relax iso 0.0 vmax 0.001
unfix           1
fix             1 all nve
""")
#        configuration_file.write('fix        2 all temp/berendsen 100 100 1\n') # si es descomenta afegir unfix           2
        configuration_file.write('timestep 0.0005\n')
        configuration_file.write("""dump            50 all xyz 10 nucleation.xyz
restart         500 nucleation.restart
run             2000
undump          50
unfix           1


fix 1 all box/relax iso 0.0 vmax 0.001
""")
        configuration_file.write('fix             2 all nve\n')
        configuration_file.write('fix             3 all temp/berendsen {0} {0} 0.025\n'.format(self.configuration.temperature))
        configuration_file.write("""timestep 0.0005
thermo 100
dump            51 all xyz 50 relax.xyz
restart         250 relax.restart
run             2500


dump  1 all xyz 1 final.xyz
run      0
undump 1
""")

    def check_atoms(self):
        """
        Function which returns the atom types and bonded atoms.

        Ha de retornar:
        a_types: llista de nombres amb els seus corresponents atomtypes
        tant per main com per incoming.
        a_coord_write: llista de totes les coordenades, incloent els shells
        num_O: quants bonds hi ha
        bonds: quin O the shells.
        """
        output_a_types = np.append(self.main_particle.atoms, self.inc_particle.atoms)
        output_a_coord = np.append(self.main_particle.coordinates, self.inc_particle.coordinates, axis=0)
        ordered_atoms = self.main_particle.ordered_atoms
        dif_atoms = self.main_particle.dif_atoms
        for i in range(len(self.inc_particle.atoms)):
            ordered_atoms[dif_atoms.index(self.inc_particle.atoms[i])].append(i+len(self.main_particle.atoms))


        for condition in self.configuration.conditions:
            if condition[0] in dif_atoms:
                if 'default' in condition:
                    for i in ordered_atoms[dif_atoms.index(condition[0])]:
                        output_a_types[i] = condition[2]

                elif condition[1] in dif_atoms:
                    min_distance = np.zeros((len(self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[0])])))
                    aux_distances = np.zeros((len(self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[1])])))
                    for i in range(len(self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[0])])):
                        for j in range(len(self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[1])])):
                            aux_distances[j] = np.linalg.norm(self.atoms.positions[self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[2])][j]]-self.atoms.positions[self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[0])][i]])
                        min_distance[i] = aux_distances.min()
                    for i in np.argpartition(min_distance, (len(self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[2])])))[0:(len(self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[2])]))]:
                        output_a_types[self.main_particle.ordered_atoms[self.main_particle.dif_atoms.index(condition[0])][i]] = condition[2]

        output_a_coord = np.array(output_a_coord)
        return output_a_types, output_a_coord
