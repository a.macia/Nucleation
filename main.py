#!/usr/bin/python3

import os
import numpy as np
import iomod
import utilities
import random as rnd

# Reading of the configuration file
configuration = iomod.parameter_reader()
configuration.read_configuration()
print(configuration.target_stoichiometry)

# Reading of input file
input_file = iomod.input_reader(configuration.input_file,
                                configuration.input_type)
if configuration.input_type == 'xyz':
    input_file.read_xyz()
# elif configuration.input_type == 'gin':
#     input_file.read_gin()
tar_stoic = configuration.target_stoichiometry
prob_coefficients = [int(tar_stoic[x]) / sum(tar_stoic.values()) for x in sorted(tar_stoic)]
add_step = 0
previous_charge = 0
non_attached_steps = 0
output_text = ""

for i in range(configuration.cycles):
    print('starting cycle', i)
    # Moving coordinates from class to variable on main
    first_particle = utilities.main_particle(atoms=np.array(input_file.atom_names),
                                             coordinates=np.array(input_file.atom_coordinates).astype(np.float))

    if np.linalg.norm(utilities.calculate_center_mass(first_particle.atoms, first_particle.coordinates)) > 0.001:
        first_particle.coordinates = first_particle.coordinates - utilities.calculate_center_mass(first_particle.atoms, first_particle.coordinates)
        print('new center of mass: ', np.linalg.norm(utilities.calculate_center_mass(first_particle.atoms, first_particle.coordinates)))
    # Generating a random coordinate and velocity for incoming particle.
    # The incoming particle should be 1st negative then positive and so on

    # Probability coefficients change
    if previous_charge < 0 :
        prob_coefficients[1] = 0
    elif previous_charge > 0 :
        prob_coefficients[0] = 0
        prob_coefficients[2] = 0
        # Select incoming particle
    monomer_toadd_pos = utilities.weighted_choice(prob_coefficients)
    monomer_toadd = utilities.monomers_list[monomer_toadd_pos]
    previous_charge = utilities.monomers_charge[monomer_toadd]

    random_vel = utilities.velocity_scale(temp=configuration.temperature, elements=monomer_toadd)
    prob_coefficients = [int(tar_stoic[x]) / sum(tar_stoic.values()) for x in sorted(tar_stoic)]

    particle_radius = np.linalg.norm(np.amax(first_particle.coordinates, axis=0))
    # The particle must be at a distance proportional to its velocity times a particle radius and security
    vector_to_center = utilities.sph2cart(utilities.random_polar_pos(particle_radius+1+random_vel*0.30))
    offset_position = utilities.set_offset(vector_to_center, particle_radius*0.9)

    name = getattr(utilities, str(monomer_toadd + '_atoms'))
    velocities = np.zeros((len(name),3))
    for j in range(len(name)):
        velocities[j, :] = -vector_to_center/np.linalg.norm(vector_to_center)*random_vel


    incoming = utilities.incoming_particle(vector=vector_to_center,
                                           velocity=configuration.velocity,
                                           offset=offset_position,
                                           dispersion=configuration.dispersion,
                                           inc_type=monomer_toadd,
                                           velocity_vector=velocities
                                           )

    add_step += 1
    num_atoms = len(first_particle.atoms) + len(incoming.atoms)
    #test = iomod.gulp_inputgen(configuration, first_particle, incoming)
    #test.md_calc()

    test = iomod.lammps_inputgen(configuration, first_particle, incoming)
    test.md_calc()
    #
    os.system('mpirun -np 4 lmp_mpi -in in.RDX')
    output_read = iomod.input_reader('final', 'xyz', num_atoms=num_atoms)
    output_read.read_xyz()
    output_read.atom_names, output_read.atom_coordinates = utilities.clean_types(configuration.conditions, configuration.shells, output_read.atom_names, output_read.atom_coordinates)
    # #Moving old calculation to new place
    if not os.path.exists('run{0}'.format(i)):
        os.makedirs('run{0}'.format(i))

    os.rename('log.lammps', 'run{0}/log.lammps'.format(i))
    os.rename('nucleation.xyz', 'nucleation{0}.xyz'.format(i))
    os.rename('coordinates.lmpdata', 'run{0}/coordinates.lmpdata'.format(i))
    os.rename('in.RDX', 'run{0}/in.RDX'.format(i))
    os.rename('relax.xyz', 'relax{0}.xyz'.format(i))
    os.rename('final.xyz', 'run{0}/final.xyz'.format(i))
    # os.rename('gulp.res', 'run{0}/restart.res'.format(i))
    # os.rename('{0}.res'.format(test.name), 'run{1}/step{1}input.res'.format(test.name, i))
    # name = 'addition{0}.xyz'.format(i)

    ## Next step comprovar que la particula esta conectada o no i si ho esta canviar d'input

    added = utilities.check_result(output_read.atom_names, output_read.atom_coordinates, incoming.atoms)
    if added is True:
        input_file = output_read
        output_read.write_xyz('step{}.xyz'.format(i))
    else:
        non_attached_steps += 1
        output_text += "Step {} gave a non connected result\n".format(i)
    # S'ha de fer el cooling de la particula


log_calc = open("nucleation.log", "w")
log_calc.write(output_text)
log_calc.write("Number of non attached steps {}".format(non_attached_steps))
log_calc.close()
