import numpy as np
import random as rnd
import math as math


class main_particle():
    """
    Main Particle definition
    """
    def __init__(self, atoms, coordinates):
        self.atoms = atoms
        self.coordinates = coordinates
        self.center_mass = calculate_center_mass(atoms, coordinates)
        self.velocities = np.zeros((len(atoms), 3))+0.00001
        self.ordered_atoms = []
        self.dif_atoms = []
        for i in range(len(atoms)):
            if atoms[i] not in self.dif_atoms:
                self.ordered_atoms.append([])
                self.dif_atoms.append(atoms[i])
                self.ordered_atoms[self.dif_atoms.index(atoms[i])].append(i)
            else:
                self.ordered_atoms[self.dif_atoms.index(atoms[i])].append(i)

        self.stoichiometry = {}
        stoichiometry = np.unique(self.atoms, return_counts=True)
        for i in range(len(np.unique(self.atoms))):
            self.stoichiometry[stoichiometry[0][i]] = int(stoichiometry[1][i])



class incoming_particle():
    """
    Particle definition of position and velocity
    """

    def __init__(self, inc_type='SiO', vector=None, velocity=None, offset=None,
                 dispersion=None, velocity_vector = None):
            self.atoms = globals()[str(inc_type + '_atoms')]
            self.coordinates = globals()[str(inc_type + '_coord')]
            self.velocities = np.zeros((len(self.atoms), 3))
            self.ordered_atoms = []
            self.dif_atoms = []
            # Assigning coordinates
            if vector is not None:
                self.coordinates = np.dot(self.coordinates, random_rotation()) + vector + offset
            # Assigning velocities
            if vector is not None and velocity_vector is None:
                self.velocities = velocity_scale(vector, velocity, dispersion, len(self.atoms))
            else:
                self.velocities = velocity_vector
            for i in range(len(self.atoms)):
                if self.atoms[i] not in self.dif_atoms:
                    self.ordered_atoms.append([])
                    self.dif_atoms.append(self.atoms[i])
                    self.ordered_atoms[self.dif_atoms.index(self.atoms[i])].append(i)
                else:
                    self.ordered_atoms[self.dif_atoms.index(self.atoms[i])].append(i)


def total_mass_calculator(atom_list):
    """Calculation of total mas of the molecule."""
    total_mass = 0.0
    for i in range(0, atom_list.shape[0]):
        total_mass += masses[atom_list[i]]
    return total_mass


def calculate_center_mass(atom_list, coordinates):
    """Calculation of the center of mass for a given set of atoms."""
    weight_coordinates = np.zeros((coordinates.shape[0], coordinates.shape[1]))
    for i in range(0, coordinates.shape[0]):
        weight_coordinates[i, :] = coordinates[i, :]*masses[atom_list[i]]
    total_mass = total_mass_calculator(atom_list)
    center_mass = np.sum(weight_coordinates, axis=0)/total_mass
    return center_mass


def random_rotation():
    """Generation of a random rotation for a molecule."""
    rnd.seed()
    vector = np.array([rnd.random(), rnd.random(), rnd.random()])
    vector = vector/np.linalg.norm(vector)
    angle = rnd.uniform(0, 2*math.pi)
    rotation = np.array([[math.cos(angle)+(vector[0]**2)*(1-math.cos(angle)),
                        vector[0]*vector[1]*(1-math.cos(angle))-vector[2] *
                        math.sin(angle),
                        vector[0]*vector[2]*(1-math.cos(angle)) +
                        vector[1]*math.sin(angle)],
                        [vector[1]*vector[0]*(1-math.cos(angle))+vector[2] *
                        math.sin(angle),
                        math.cos(angle)+(vector[1]**2)*(1-math.cos(angle)),
                        vector[1]*vector[2]*(1-math.cos(angle)) -
                        vector[0]*math.sin(angle)],
                        [vector[2]*vector[0]*(1-math.cos(angle))-vector[1] *
                        math.sin(angle),
                        vector[2]*vector[1]*(1-math.cos(angle)) +
                        vector[0]*math.sin(angle),
                        math.cos(angle)+(vector[2]**2)*(1-math.cos(angle))]])
    return rotation


def sph2cart(vector_sph):
    vector_cart = np.zeros(3)
    vector_cart[0] = vector_sph[0]*math.sin(vector_sph[1])*math.cos(vector_sph[2])
    vector_cart[1] = vector_sph[0]*math.sin(vector_sph[1])*math.sin(vector_sph[2])
    vector_cart[2] = vector_sph[0]*math.cos(vector_sph[1])
    return vector_cart


def random_polar_pos(distance):
    polar_vector = np.array([distance, rnd.uniform(0, 2*math.pi), rnd.uniform(0, math.pi)])
    return polar_vector


def velocity_scale(mean_velocity=None, dispersion=None, number_elements=None, temp=None, elements=None):
    from scipy.stats import maxwell
    elements = globals()[str(elements + '_atoms')]
    if temp is None:
        random_vel = rnd.normalvariate(mean_velocity, dispersion)

    else:
        number_elements = len(elements)
        mass = 0
        # Calculating a parameter
        for i in elements:
            mass += masses[i]
        mass = mass/6.023e23
        a_param = math.sqrt(1.380648e-23*temp/(mass*1e-3))
        random_vel = (maxwell.rvs(scale=a_param)*1e-2)
        min_vel = maxwell.interval(0.10, scale=a_param)[0]*1e-2
        max_vel = maxwell.interval(0.50, scale=a_param)[1]*1e-2
        while random_vel < min_vel or random_vel > max_vel:
            random_vel = (maxwell.rvs(scale=a_param)*1e-2)
#        vel_vector = -vector/np.linalg.norm(vector)*random_vel

    # scaled_vector = np.zeros((number_elements, 3))
    # for i in range(number_elements):
    #     scaled_vector[i, :] = vel_vector

    return random_vel

def clean_types(conditions, shells, atom_list, atom_coordinates):
    cp_al = []
    cp_ac = []
    for i in range(len(atom_list)):
        for condition in conditions:
            if atom_list[i] in condition:
                cp_al.append(condition[0])
                cp_ac.append(atom_coordinates[i])
    # atom_list = np.array(cp_al)
    # print(np.array(cp_ac))
    # cp_ac = np.array(cp_ac)
        ## GULP take away O1 and O2
        # new = ''.join([i for i in atom_list[i] if not i.isdigit()])
        # atom_list[i] = new
    return cp_al, cp_ac


def check_result(atom_names, atom_coordinates, inc_atoms):
    connected = False
    atom_coordinates = np.array(atom_coordinates, dtype = 'float')
    final_position = len(atom_names)-len(inc_atoms)
    for i in range(final_position):
        dist = np.linalg.norm(atom_coordinates[i,:]-atom_coordinates[len(atom_names)-1,:])
        if dist < 3.0:
            connected = True
            break
    if connected is False:
        print("Inc Particle not connected, redoing step")
    return connected
#### Versio anterior.
# def check_result(atom_names, atom_coordinates):
#     rows_toerase=[]
#     for i in range(len(atom_names)):
#         attached = False
#         vector1 = np.array(atom_coordinates[int(i)]).astype(np.float)
#         for j in range(len(atom_names)):
#             if j != i:
#                 vector2 = np.array(atom_coordinates[int(j)]).astype(np.float)
#                 if np.linalg.norm(vector1-vector2) <= 3.0:
#                     attached = True
#                     break
#         if attached == False:
#             print('found atom not attached to the particle')
#             rows_toerase.append(i)
#     print('Erasing rows:', rows_toerase)
#
#     for element in range(len(rows_toerase)):
#         atom_names = np.delete(atom_names, (element), axis=0)
#         atom_coordinates = np.delete(atom_coordinates, (element), axis=0)
#     return atom_names, atom_coordinates


def set_offset(vector, max_distance):
    """Return a vector with a random offset.

    This function will generate a vector with a random offset with respect to
    the center. Therefore it requires a vector to translate and a radii for
    the max value to translate. Two possible aproaches:
        1. Obtain a point (P) in a circle using circle equation.
        2. Reescale it's vector randomly from 0 to max distance of the particle
        3. Convert vector to original coordinates
        3.1. Calculate the angle between Z axis and vector (alpha)
        3.2. Calculate perpendicular vector (R) between Z axis and vector
        3.3. Do rotation around (R) with angle alpha of the point (P)
    return a vector with
    """
    # 1. Point of a circle
    x_pos = np.random.uniform(-1, 1)
    y_pos = math.sqrt(1-x_pos**2)
    if np.random.uniform(0, 1) > 0.5:
        y_pos = -y_pos
    point = np.array([x_pos, y_pos, 0.0])
    # 2. Reescale
    point = point * np.random.uniform(0, max_distance)
    # 3. 1
    angle = math.acos(np.dot(vector, np.array([0.0, 0.0, 1.0]))/np.linalg.norm(vector))
    # 3. 2
    perp_vect = np.array([vector[1], -vector[0], 0.0])/np.linalg.norm(np.array([vector[1], -vector[0], 0.0]))
    # 3. 3
    rotation = rotation_matrix(angle, perp_vect)
    point = np.dot(point, rotation)
    return point

def weighted_choice(weights):
    totals = []
    running_total = 0

    for w in weights:
        running_total += w
        totals.append(running_total)

    random = rnd.random() * running_total
    for i, total in enumerate(totals):
        if random < total:
            return i

def rotation_matrix(angle, vector):

    rotation = np.array([[math.cos(angle)+(vector[0]**2)*(1-math.cos(angle)),
                        vector[0]*vector[1]*(1-math.cos(angle))-vector[2] *
                        math.sin(angle),
                        vector[0]*vector[2]*(1-math.cos(angle)) +
                        vector[1]*math.sin(angle)],
                        [vector[1]*vector[0]*(1-math.cos(angle))+vector[2] *
                        math.sin(angle),
                        math.cos(angle)+(vector[1]**2)*(1-math.cos(angle)),
                        vector[1]*vector[2]*(1-math.cos(angle)) -
                        vector[0]*math.sin(angle)],
                        [vector[2]*vector[0]*(1-math.cos(angle))-vector[1] *
                        math.sin(angle),
                        vector[2]*vector[1]*(1-math.cos(angle)) +
                        vector[0]*math.sin(angle),
                        math.cos(angle)+(vector[2]**2)*(1-math.cos(angle))]])

    return rotation

# Definitions of molecules

masses = {'Si': 28.085, 'O': 15.999, 'H': 1.008, 'Mg': 24.305}
SiO_coord = np.array([[-0.54667121, 0.0, 0.0], [0.95963879, 0.0, 0.0]])
SiO_atoms = np.array(['Si', 'O'])
Mg_coord = np.array([[0.0, 0.0, 0.0]])
Mg_atoms = (['Mg'])
O_coord = np.array([[0.0, 0.0, 0.0]])
O_atoms = (['O'])
H2O_coord = np.array([[0.000, 0.06645392, 0.000], [0.738661, -0.52737908, 0.000],
                     [-0.738661, -0.52737908, 0.000]])
H2O_atoms = np.array(['O ', 'H ', 'H '])
monomers_list = ['Mg', 'O', 'SiO']
monomers_charge = {'SiO': 2, 'O': -2, 'Mg': 2}

# Test Zone
#test_particle = incoming_particle()
